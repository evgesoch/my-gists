"""Transform the coordinates of a geojson file from one reference system to another using `pyproj` library."""

import json
from functools import lru_cache, _lru_cache_wrapper
from pyproj import Transformer


def transform_coordinates_with_cached_transformer(TransformerFromCRS: _lru_cache_wrapper, x_coordinate: float, y_coordinate: float) -> tuple[float, float]:
    """Transform coordinates with caching the Transformer object outside of this function.

    Before calling the function, call this:

    ```
    TransformerFromCRS = lru_cache(Transformer.from_crs)
    ```

    Then pass `TransformerFromCRS("EPSG:aaaa", "EPSG:bbbb")` as argument into this function.

    Args:
        TransformerFromCRS (_lru_cache_wrapper): The cached transformer function object
        x_coordinate (float): The x coordinate
        y_coordinate (float): The y coordinate

    Returns:
        tuple[float, float]: The converted coordinates
    """

    x, y = TransformerFromCRS.transform(x_coordinate, y_coordinate)

    return x, y


def transform_coordinates(TransformerFromCRS: _lru_cache_wrapper, coords: list) -> list:
    """Utility function to transform coordinates. Coordinate reference systems are declared with `EPSG:aaaa` strings
    when creating the `TransformerFromCRS`

    Args:
        TransformerFromCRS (_lru_cache_wrapper): The cached transformer function object
        coords (list): A list with the x and y coordinates

    Returns:
        list: A list with the transformed coordinates
    """

    x, y = transform_coordinates_with_cached_transformer(TransformerFromCRS, coords[0], coords[1])
    return [x, y]


if __name__ == "__main__":
    # Define the coordinate reference systems
    FROM_CRS = "EPSG:3857"
    TO_CRS = "EPSG:4326"

    # Create cached transformer object to speed up coordinate transformations
    TransformerFromCRS = lru_cache(Transformer.from_crs)

    with open('/src/input-geojson.json', 'r') as f:
        geojson_data = json.load(f)

        for feature in geojson_data['features']:
            geometry_type = feature['geometry']['type']
            coordinates = feature['geometry']['coordinates']

            if geometry_type == 'Point':
                feature['geometry']['coordinates'] = transform_coordinates(TransformerFromCRS(FROM_CRS, TO_CRS), coordinates)
            elif geometry_type == 'LineString' or geometry_type == 'MultiPoint':
                feature['geometry']['coordinates'] = [transform_coordinates(TransformerFromCRS(FROM_CRS, TO_CRS), coord) for coord in coordinates]
            elif geometry_type == 'Polygon' or geometry_type == 'MultiLineString':
                feature['geometry']['coordinates'] = [[transform_coordinates(TransformerFromCRS(FROM_CRS, TO_CRS), coord) for coord in part] for part in coordinates]
            elif geometry_type == 'MultiPolygon':
                feature['geometry']['coordinates'] = [[[transform_coordinates(TransformerFromCRS(FROM_CRS, TO_CRS), coord) for coord in part] for part in poly] for poly in coordinates]

        with open('/src/output-geojson.json', 'w') as f:
            json.dump(geojson_data, f)
