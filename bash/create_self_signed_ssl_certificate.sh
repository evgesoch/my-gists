#!/bin/bash

# Generate a SSL certificate without password for 1 year
#
# C: Country Name (2 letters, optional)
# ST: State (state, optional)
# L: Locality Name (city, optional)
# O: Organization Name (company, optional)
# OU: Organizational Unit Name (section inside company, optional)
# CN: Common Name (required)
# emailAddress: The Email Address (optional)
openssl req -x509 -nodes -new -keyout server_domain_name.key -out server_domain_name.crt -days 365 -subj "/C=/ST=/L=/O=/OU=/CN=www.server_domain_name.com/emailAddress="
